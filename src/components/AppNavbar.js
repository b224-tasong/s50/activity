// Old practice in importing components
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';

// Better practice in importing components
import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext'



export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return (

		<Navbar bg="light" expand="lg">
				      <Container>
				        <Navbar.Brand as={Link} to="/" href="#home">React-Bootstrap</Navbar.Brand>
				        <Navbar.Toggle aria-controls="basic-navbar-nav" />
				        <Navbar.Collapse id="basic-navbar-nav">
				          <Nav className="me-auto">
				            <Nav.Link as={Link} to="/" href="#home">Home</Nav.Link>
				            <Nav.Link as={Link} to="/courses" href="#link">Courses</Nav.Link>
				            {
				            	(user.id !== null) ?
				            		<Nav.Link as={Link} to="/logout" href="#link">Logout</Nav.Link>
				            			
				            		:

				            		<>
				            			<Nav.Link as={Link} to="/login" href="#link">Login</Nav.Link>
				            			<Nav.Link as={Link} to="/register" href="#link">register</Nav.Link>
				            		</>
				            }
				          </Nav>
				        </Navbar.Collapse>
				      </Container>
		</Navbar>


		)
}