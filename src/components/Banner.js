import { Row, Col, Button } from 'react-bootstrap';
import Error from '../pages/Error'
import { Link } from 'react-router-dom';


export default function Banner(props) {

	// console.log(props.isWrongPath)

	return(
			<>
				{
					(props.isWrongPath) ?
						<>
							<Row>
								<Col>
									<h1>Error 404 - Page Not Found</h1>
									<p>This page can't be found!</p>
									<Button variant='primary' as={Link} to="/">Back to Home</Button>
								</Col>
							</Row>
						</>	
						:
						<Row>
							<Col>
									<h1>Zuitt Coding Bootcamp</h1>
									<p>Opportunities for everyone, everywhere.</p>
									<Button variant='primary'>Enroll Now!</Button>
							</Col>
						</Row>
				}
			</>
		)
}







// function App() {
//   return (
//     <Router>
//         <AppNavbar />
//         <Container>
//             <Routes>
                
//                 <Route path="/" element={<Home isWrongPath={false}/>} />
//                 <Route path="/courses" element={<Courses/>}/>
//                 <Route path="/register" element={<Register/>}/>
//                 <Route path="/login" element={<Login/>}/>
//                 <Route path="/logout" element={<Logout/>}/>
//                 <Route path="*" element={<Home isWrongPath={true}/>} />
//             </Routes>
//         </Container>
//     </Router>
    
//   );
// }

// finish the code solution
// import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';
// import Error from '../pages/Error'
// export default function Home(props) {
// 	return(
// 			<>
// 				{
// 					(props.isWrongPath) ?
// 						<Error />
// 						:
// 						<Banner /> 
// 				}
// 				<Highlights />
// 				{/*<CourseCard/>*/}
// 			</>
// 		)
// }