import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard';
import Highlights from '../components/Highlights';


export default function Home(props) {

	<h1>
	{props.isWrongPath}
	</h1>
	return(

			<>
				<Banner isWrongPath={props.isWrongPath}/>   
				<Highlights />
				{/*<CourseCard/>*/}
			</>
		)
}