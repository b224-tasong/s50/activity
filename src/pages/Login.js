import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login() {


	// This one allows as to consum the user context object and its properties to be used for validation.
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);



	function loginUser(e) {
		
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			// We will receive either a token or a false response.
			console.log(data)

			if (typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access )
				retrieveUserDetails(data.access)
				Swal.fire({
					title: 'Login Successfully',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
				return <Navigate to="/courses.js" />
			}else{
				Swal.fire({
					title: 'Authentication Failed!',
					icon: 'error',
					text: 'Incorrect Email or Password!'

				})
			}

			
		})


		// if (email === userName && password === pass) {
			
		// 	// Clearing the input fields and states
		// 	setEmail('');
		// 	setPassword('');
		// 	alert('You are now logged in');
		// 	setIsActive(true);
		// 	// Setting the user to active
		// 	setUser(true);
		// 	// Redirecting the user to the Courses.js page
		// 	return <Navigate to="/courses.js" />
		// }else{
		// 	alert('Incorrect username or password!');
		// }

		// Set email of the authenticated user in the local storage
		/*
			Syntax:
			localStorage.setItem("propertyName", value)
		*/
		// localStorage.setItem("email", email)
		// Sets the global yser state to have properties obtained from local storage.
		// setUser({email: localStorage.getItem('email')});
	}


	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${token}`
			}

		})
		.then(res => res.json())
		.then(data=> {
			console.log(data);
			// Global user state for validataion accross the whole app

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	useEffect(()=>{
		if (email !== "" && password !== "") {
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password]);

	const MyInput = () => {
	  const [value, setValue] = useState('');

	  useEffect(() => {
	    setValue(value.replace(/[^0-9]/g, ''));
	  }, [value]);

	  return <input type="tel" onInput={e => setValue(e.target.value)} value={value} />
	} 

	return(

			(user.id !== null) ?
				<Navigate to="/courses" />
				:
				<Form className="mt-5" onSubmit={e => loginUser(e)}>
				      <Form.Group className="mb-3" controlId="userEmail">
				      		<Form.Label>Email address</Form.Label>
				      		<Form.Control type="email"
				      			placeholder="Enter email" 
				      			value={email}
				      			onChange={e => setEmail(e.target.value)}
				      			required
				      		/>
				      </Form.Group>

				       <Form.Group className="mb-3" controlId="password">
				      		<Form.Label>Password</Form.Label>
				      		<Form.Control 
				      			type="password" 
				      			placeholder="Password" 
				      			value={password}
				      			onChange={e => setPassword(e.target.value)}
				      			required
				      		/>
				      </Form.Group>
				      {
				      	isActive ?
				      	<Button variant="success" type="submit" id="submitBbtn">
				      	  Submit
				      	</Button>
				      	:
				      	<Button variant="success" type="submit" disabled>
				      	  Submit
				      	</Button>
				      }
				</Form>

		)

}